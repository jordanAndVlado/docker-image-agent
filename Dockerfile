FROM jenkins/agent:alpine-jdk11

USER root
RUN apk add --no-cache curl ca-certificates git unzip docker
RUN apk add --update nodejs npm
ARG HELM_VERSION=3.7.0
RUN addgroup -g 115 dockergroup && adduser jenkins dockergroup

ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION_1 18.16.1
ENV NODE_VERSION_2 16.18.0

RUN mkdir -p $NVM_DIR \
    && chown -R jenkins:jenkins $NVM_DIR \
    && curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION_1 \
    && nvm install $NODE_VERSION_2

ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION_1/bin:$PATH


RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 \
    && chmod 700 get_helm.sh \
    && ./get_helm.sh \
    && helm version


ARG KUBECTL_VERSION=v1.22.0

RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && kubectl version --client


ARG SONAR_SCANNER_VERSION=4.6.2.2472

RUN curl --create-dirs -sSLo /tmp/sonar-scanner.zip https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${SONAR_SCANNER_VERSION}.zip \
    && unzip /tmp/sonar-scanner.zip -d /opt \
    && rm /tmp/sonar-scanner.zip \
    && ln -s /opt/sonar-scanner-${SONAR_SCANNER_VERSION}/bin/sonar-scanner /usr/local/bin/sonar-scanner \
    && sonar-scanner --version

USER jenkins
